# README #
this is just a placeholder to redirect to GitHub, since they have deleted our old Mercurial repository. 

To find time series classification code described in this paper
https://link.springer.com/article/10.1007/s10618-016-0483-9 
go here for Weka compatible Java version 
https://github.com/uea-machine-learning/tsml
or here for scikit-learn compatible Java version
https://github.com/alan-turing-institute/sktime

if you really want the old code, get in touch with Tony Bagnall (ajb at uea.ac.uk)